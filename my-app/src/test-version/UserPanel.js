import React from "react"
//import dp from "/home/ashiq/Documents/MAPATHON/frontendlearning_icfoss/my-app/src/assets/boy.jpg"
import {Card} from "react-bootstrap"
import dp from "/home/ashiq/Documents/MAPATHON/frontendlearning_icfoss/my-app/src/assets/profile.svg"
import icon from "/home/ashiq/Documents/MAPATHON/frontendlearning_icfoss/my-app/src/assets/graph1.svg"
import "./style.css"


const UserPanel = props => (
                <Card className="grade2 align-items-center shadow p-2 mb-3 ml-1 bg-white">
                    <Card.Header className="text-white">My Dashboard</Card.Header>
                   <div className="circle-image">
                        <img alt="DP" className="align-items-center" src={dp} style={{width:'100%'}}/>
                   </div>                 
                            <Card.Body>
                                        <Card.Title className="text-white text-center">{props.name}</Card.Title>
                                        <Card.Subtitle className="text-secondary text-center">OSM#{props.osmID}</Card.Subtitle>
                            </Card.Body>          
                            
                            <div className="container">
                                <div className="row">
                                    <div className="col-xl-5 col-lg-5 col-md-5 col-sm-4">
                                        <div className="text-center font-weight-bold text-white list-group">
                                            <div className="list list-group-item">RANK</div>
                                            <div className="rank list list-group-item">{props.rank}</div>

                                        </div>
                                    </div>
                               
                                    <div className="col-xl-7 col-lg-7 col-md-7 col-sm-4">
                                      <div className="text-center font-weight-bold text-white list-group">
                                            <div className="list list-group-item">SCORE</div>
                                            <div className="score list list-group-item">{props.score}</div>

                                      </div>
                                           
                                    </div>
          
                                  </div>
                               
                                </div>
    
                          <div>
                            
                             <div className="d-flex justify-content-center">
                                <img alt="graph" src={icon} style={{width: '25%', position: 'relative'}} />
                             </div>
                          </div>    
                </Card>
                
            
       
)
    



export default UserPanel