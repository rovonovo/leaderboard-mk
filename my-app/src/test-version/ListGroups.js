import React from "react"
import image from "/home/ashiq/Documents/MAPATHON/frontendlearning_icfoss/my-app/src/assets/icon.png"
import Container from "react-bootstrap/Container"
import crown from "/home/ashiq/Documents/MAPATHON/frontendlearning_icfoss/my-app/src/assets/crown.svg"
import "./style.css"

function ListGroups(props) {
let winner_style = {
    width: "2.3rem",
   

}

return(
 <Container>

    <div className="media p-1 m-4" style={props.style}>
    <div className="text-wrap align-items-center mb-1 pb-3 pr-1 bg-light-secondary">
         {props.rankStyle === true 
             ? <img src={crown} alt="crown" style={winner_style}  />
            : <p className ="font-weight-bold" style={props.rankStyle}>#{props.rank}</p>  
          }
         
        </div>
    <img src={image} alt="profile" className="mr-2 mt-1 mt-2 rounded-circle" style={{width:'50px'}} />
     <div className="media-body mt-1">
        <div className="row ml-2" >

            <div className="col-sm-12 col-md-6 col-lg-6 font-weight-bold"> {props.player}
                <div className="row-2 mt-1 text-secondary">OSM#{props.osmID}</div>
            </div>
            <div className="col-sm-12 col-md-6 col-lg-6">
                <div  className="offset-lg-4 offset-md-3">
                    <div className="row">
                     <div className="col-12 font-weight-bold" style={{fontSize: '1.4rem'}}>{props.score}</div>
                        <div className="col-12 h6 text-secondary" style={{opacity: '0.5'}}>
                            points
                        </div>

                    </div>
                    
                </div>
            </div>
        </div>
     </div>
   </div>
  
  
  </Container>
)
}

export default ListGroups