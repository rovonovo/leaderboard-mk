import React from "react"
import {Card, Col, Row} from "antd"
import {TeamOutlined, ContactsOutlined, UserOutlined } from "@ant-design/icons"

function CampDetails(props){
const {Meta} = Card

    return(
       
       <Card style={{ width: 220, margin: 15, border:'1px solid #FFFF', backgroundColor: 'transparent' }} >
            <Row>
            <Col span={15} >
                <Meta
                title={<span className="text-white" style={{fontSize:'0.9rem'}}>{props.title}</span>}
                description={<span className="text-white font-weight-bold" style={{fontSize:'1rem'}}>{props.description}</span>}

                />
            </Col>
            <Col span={3} offset={6}>
                {props.title === "Team" ? <ContactsOutlined style={{ fontSize: '1.2rem', color: '#FFFF'}}/>
                    : <UserOutlined style={{ fontSize: '1.2rem', color: '#FFFF'}}/>
                }
            </Col>

            </Row>
      </Card>

      
    )
}

export default CampDetails