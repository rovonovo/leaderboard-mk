import React,{useState} from "react"
import {Input, AutoComplete, Row, Col, Button} from "antd"
import {SearchOutlined} from "@ant-design/icons"
import {data} from "./data"
import {Colors }from "../../Colors"
import UserCard from "../header/UserCard"
import CampDetails from "./CampDetails"

const mockVal = (str) => {  //function to check searched string with json data and return new array with refined names
    let suggestion = ""
    let newjson = []
    //let arr = data.map((item) => item.username)[0].toString()
    //console.log(arr)
  for(var i =0; i< data.length; i++){
       let arr = data.map((item) => item.username)[i].toString()
        if(arr.toUpperCase().includes(str.toUpperCase())){
            suggestion = arr
           newjson.push({value: suggestion})
           
        }
        //console.log(newjson)
    }
    
    return newjson
}
    





function Header(props){
    const [value, setValue] = useState('')
    const [options, setOptions] = useState([])
    const [embed, setEmbed ] = useState(false)
    //console.log(options)

  
    //const v = [{value: 'ash'}, {value: 'bss'}, {value: 'gaf'}]
    
const onSearch = searchText => {
    setOptions(
        !searchText ? []: mockVal(searchText))
}

const onSelect = data => {
    setEmbed(true)
    setValue(data)
    console.log('OnSelect', data) 
 
}

const onChange = data => {
    //setValue(data)

}

return (
    <div style={{backgroundColor: `${Colors.primary}`, margin:'2px', padding: '2px', borderRadius: '6px'}}>
    <Row gutter={2}>
        <Col span={7} className="d-flex align-items-center m-2">
            <AutoComplete 
                options={options}
                style={{
                    width: 300,
                    
                }}
                onSelect={onSelect}
                onSearch={onSearch}  
                placeholder="Search with username"
            >

                
            </AutoComplete>

            <Button  shape="square" icon={<SearchOutlined style={{color: "#FFFF", fontSize:'1.4rem'}}/>} 
            style={{backgroundColor:`${Colors.secondary}`, border:'none', width:'3.5rem'}} 
            
            />
        </Col>
       <Col span={4}>
            { embed &&
                data.map((item) => {
                    if(item.username === value)
                    return ( 
                       <UserCard 
                           name={item.username}
                           score={item.score}
                           rank={item.rank}
                       /> 
                               
                    )} )
             }       
       </Col>
       <Col span={3} offset={3}>
             <CampDetails 
                 title="Team"
                 description="Campaign"
             />
       </Col>
       <Col span={3 } offset={1}>
            <CampDetails 
                 title="Total Members"
                 description="18"
            />
       </Col>
 
     </Row>
     </div>
)

}

export default Header
