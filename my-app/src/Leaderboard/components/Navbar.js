import React from "react"
import {Col, Row} from "antd"
import logo from "assets/mapathon_log.svg"
import {AlignRightOutlined} from "@ant-design/icons"

function Navbar() {
    return(
        <>
            <Row gutter={{xs: 40, sm: 32 , md: 32, lg: 24, xl: 16 }} className="m-2 pt-1">
                <Col xs={8} sm={8}  md={6} lg={4} xl={4} >
                    <img  src={logo} style={{width:'calc(120px + 2vw)'}}/>
                </Col>
                <Col xs={{span:10}} sm={10} md={6} lg={6} xl={5}>
                    <p className="font-weight-bold" style={{fontSize:'calc(16px + 1vw)'}}>Leaderboard</p>
                </Col>
                <Col xs={{span:1 , offset:2}} sm={{span:1, offset:4}} lg={{span:1, offset:14}} xl={{span:1, offset:14}}  className="d-flex justify-content-end pt-1">
                      <AlignRightOutlined style={{fontSize: '1.6rem'}} />
                </Col>
            </Row>
        </>
    )
}

export default Navbar