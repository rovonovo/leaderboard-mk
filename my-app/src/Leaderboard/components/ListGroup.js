import React from 'react'
import { List, Avatar, Space, Tag , Row, Col} from 'antd';
import { UserOutlined} from '@ant-design/icons';
import {data} from "./header/data"
import {Colors} from "../Colors"
import profile from "assets/profile.svg"


function ListGroup(props){
const score="8"

return(
    <div>
  <List style={{width:370, marginTop: 16}}
    itemLayout="vertical"
    size="small"
    pagination={{
      onChange: page => {
        console.log(page);
      },
      pageSize: 6,
    }}
    dataSource={data}
  
    renderItem={item => (
      <List.Item
        key={item.id}
      
        extra={
            
                <Tag className="mt-1 pt-1" color={Colors.base} style={ score.length <= 4 ? {width:'3rem'} : '' }>
                    <h6 className="d-flex align-items-center justify-content-center font-weight-bold"
                        style={{color: `${Colors.primary}`}}
                    >
                    {item.rank}
                    </h6>
                </Tag>
                
        }
      >
        <List.Item.Meta

          avatar={<Avatar 
                 size={50}
                 src={profile}
                 style={{
                        backgroundColor: `${Colors.base}`,
                        color: `${Colors.primary}`,
                        fontSize: '1.6rem',
                        
                    }}
            //icon={ }

            /> }
          title={<span className="font-weight-bold text-black" style={{fontSize:'0.9rem'}}>{item.username}</span>}
          description={<span className="font-weight-bold text-black" style={{fontSize:'1rem'}}>{item.score}</span>}
        />
       
      </List.Item>
    )}
  />
    </div>

)

    }

    export default ListGroup