import React from 'react'
import {Button, Row, Col } from 'antd'
import WinnersPanel from "../components/WinnersPanel"
import Navbar from "../components/Navbar"
import ListGroup from "../components/ListGroup"
import { data } from "../components/header/data"
import Header from "../components/header"

function Main() {
    return (
      <div className="container-fluid">
            <Navbar />
            <Header />
            <Row>
            
            <Col lg={{span: 13}} xl ={{span: 15}} className="mt-4 pt-3 ml-2">
                      <h4 className="font-weight-bold text-black-50">Top Achievers</h4>
                      <WinnerSection />
                </Col>
                <Col lg={{span: 7, offset: 1}} xl={{span:7, offset: 1}} className="mt-4 pt-3 mr-2">
                    <h4 className="font-weight-bold text-black-50 ">All Members</h4> 
                    <ListGroup />
                </Col>
            </Row>
      </div>
    )
}

export default Main  

const WinnerSection = () => (
    <div className="m-2 mt-4 pt-3">
    <Row gutter={10}>
        <Col>
            {data.map((item) => {
                if(item.rank === "1" ){
                    return(
                        <WinnersPanel 
                            name={item.username}
                            score={item.score}
                            rank={item.rank}
                           
                        />
                    )
                }
            })}
        </Col>
        <Col>
        {data.map((item) => {
                if(item.rank === "2" ){
                    return(
                        <WinnersPanel 
                            name={item.username}
                            score={item.score}
                            rank={item.rank}
                        />
                    )
                }
            })}
        </Col>
        <Col>
        {data.map((item) => {
                if(item.rank === "3" ){
                    return(
                        <WinnersPanel 
                            name={item.username}
                            score={item.score}
                            rank={item.rank}
                        />
                    )
                }
            })}
        </Col>
    </Row>
 
</div>
)