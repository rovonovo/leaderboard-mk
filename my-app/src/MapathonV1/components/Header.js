import React from 'react'
import Media from "react-bootstrap/Media"
import logo from "assets/mapathon_log.svg"
import {useMediaQuery} from "react-responsive"
import "./col.css"

function Header() {
    const isLargeScreen = useMediaQuery({
        query : '(min-device-width : 1900px)'
    })
    
   let styles={
        letterSpacing: '3px',
        fontFamily: 'open-sans, sans-serif, Times New Roman, Courier',
        backgroundImage : 'linear-gradient(to left,#071929 10%,#1e7768 90%)'
       
        
    }
    return (
        <div>
            <header className="d-block p-2" style={styles}>
                <Media>
                <img
                    width={62}
                    height={56}
                    className="mr-3"
                    src={logo}
                    alt="Logo"
                />
                    <Media.Body >
                        <p className="text-white" style={isLargeScreen ? {fontSize : '1.4rem'} : {fontSize: '1.2rem'}} >Mapathon Keralam</p>
                        <p className="text-white" style={isLargeScreen ? {opacity: '0.6', fontSize : '1.2rem'} : {opacity: '0.6',fontSize: '1rem'}}>Leaderboard</p>
                    </Media.Body>
                </Media>
            </header>
        </div>
    )
}

export default Header

