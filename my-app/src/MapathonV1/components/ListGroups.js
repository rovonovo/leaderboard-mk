import React,{useState} from "react"
import image from "assets/profile3.svg"
import Container from "react-bootstrap/Container"
import crown from "assets/crown.svg"
import {useMediaQuery} from "react-responsive"
import {useSpring, animated} from "react-spring"
import coin from "assets/coin.svg"


function ListGroups(props) { 
    const isLargeScreen = useMediaQuery({
        query: '(min-width: 1900px)'
      })

      const render = useSpring({
        config: {duration: 1200, mass:10, tension:50, friction:10},
        to: {
             transform : 'translate3d(0, 0, 0)'  },
        from : {
                transform : 'translate3d(100px,-0,0)'}
    })


    let winner_style = {
        normal: {width: "2.3rem"},
        large:  {width: "2.6rem"}
       
    }
    let player_style = {
        normal: { letterSpacing:'1px', fontSize:'1.1rem', fontWeight: 'bold'},
        large :  { letterSpacing:'2px', fontSize:'1.6rem', fontWeight: 'bold'}
    }
    let osm_style = {
        normal: { fontSize:'1rem', fontWeight: 'bold'},
        large :  { fontSize:'1.3rem', fontWeight: 'bold'}
    }
    let score_style = {
        normal: {fontSize: '1.3rem',color:'#4db002',fontWeight: 'bold'},
        large :  {fontSize: '1.6rem',color:'#4db002', fontWeight: 'bold'}
    }
    

return(
 <Container >
    <animated.div style={render}>
    <div className="media p-1 m-4 extra" style={props.style}>
    <div className="text-wrap align-items-center mb-1 pb-3 pr-1 bg-light-secondary">
         {props.rankStyle === true 
             ? <img src={crown} alt="crown"  style={isLargeScreen ? winner_style.large : winner_style.normal}  />
            : <p className ="m-2 mb-2 font-weight-bold" style={props.rankStyle}>{props.rank}</p>  
          }
         
        </div>
    <img src={image} alt="profile" className="m-1 mt-2 mr-2 p-1 rounded-circle" style={isLargeScreen? {width:'50px'}: {width: '40px'}} />
     <div className="media-body mt-1">
        <div className="row ml-2" >

            <div className="col-sm-12 col-md-6 col-lg-6"  style={isLargeScreen ? player_style.large : player_style.normal}> {props.player}          
                  <div className="row-2 mt-1 text-secondary" style={isLargeScreen ? osm_style.large : osm_style.normal}>OSM#{props.osmID}</div>
            </div>
            <div className="col-sm-12 col-md-6 col-lg-6">
                <div  className="offset-lg-1 offset-xl-2 offset-md-2">
                    <div className="row">
                     
                     <div className="col-xl-2 col-lg-1 col-sm-12">

                                <img src={coin} style={isLargeScreen ? {width: '1.3rem', marginTop: '6px'} : {width: '1.1rem'}  }/>

                     </div>
                     <div className="col-xl-7 col-lg-6 col-sm-12 order-first order-lg-0" style={isLargeScreen ? score_style.large : score_style.normal}>{props.score}</div>

                    </div>
                    
                </div>
            </div>
        </div>
     </div>
     </div>
   </animated.div>
  
  
  </Container>
)
}

export default ListGroups

/** <div  className="offset-lg-4 offset-md-3">
                    <div className="row">
                     <div className="col-12 font-weight-bold" style={{fontSize: '1.4rem'}}>{props.score}</div>
                        <div className="col-12 h6 text-secondary" style={{opacity: '0.5'}}>
                            points
                        </div>

                    </div>
                    
                </div> */