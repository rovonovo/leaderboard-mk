import React from "react"
import {useMediaQuery} from "react-responsive"

function CampName(props) {

    const isLargeScreen = useMediaQuery({
        query: '(min-device-width: 1900px)'
      })

    let head_style = {
        normal : {color:'#2bb31f', fontSize: '2rem', fontWeight: 'bold' , fontFamily: '"Courier New", Courier, monospace'},
        large : {color:'#2bb31f', fontSize: '2.8rem',  fontWeight: 'bold', fontFamily: '"Courier New", Courier, monospace'}
      }

    return(
        <div className="d-block text-center m-3 pt-3"> 
          <p  style={isLargeScreen ? head_style.large : head_style.normal}>{props.name.toUpperCase()}</p>  
          </div>
    )
}

export default CampName