import React, {useState} from "react"
import {Card} from "react-bootstrap"
import dp from "assets/profile.svg"
import "./style.css"
import icon from "assets/graph1.svg"
import {useMediaQuery} from "react-responsive"


function UserPanel(props){

  const isLargeScreen = useMediaQuery({ //to check if screen size is Large
    query: '(min-width: 1900px)'
  })
  
  let player_style=  {
    normal: {fontSize:'1.2rem',color: '#fcfbdb'},
    large :  {fontSize:'1.8rem',color: '#fcfbdb'}
  }
  let osm_style = {
    normal: { fontSize:'1.1rem', fontWeight: 'bold'},
    large :  { fontSize:'1.6rem', fontWeight: 'bold'}
}
let score_style = {
  normal: {fontSize: '1.8rem',color:'#14c048'},
  large :  {fontSize: '2.7rem',color:'#14c048'}
}
let label = {
  normal: {fontSize: '1rem' },
  large :  {fontSize: '1.3rem'}
}


               return( 
               <Card className="grade2 align-items-center shadow p-4 mb-3 ml-1 bg-white" style= {isLargeScreen ? {height: '45rem', borderRadius: '9%'}: {borderRadius: '6%'}}>
                    <Card.Header className="text-white" style={isLargeScreen ? label.large : label.normal}>My Dashboard</Card.Header>
                   <div className="circle-image">
                        <img alt="DP" className="d-flex justify-content-center" src={dp} style={{width: '100%'}}/>
                   </div>                 
                            <Card.Body style={isLargeScreen ? {marginTop: '10px', paddingTop: '20px'} : {marginTop: '3px', paddingTop: '6px'}}>
                                        <Card.Title className="text-center" style={isLargeScreen ? player_style.large : player_style.normal}>{props.name.toUpperCase()}</Card.Title>
                                        <Card.Subtitle className="text-secondary text-center" style={isLargeScreen ? osm_style.large : osm_style.normal} >OSM#{props.osmID}</Card.Subtitle>
                            </Card.Body>          
                            
                            <div className="container">
                                <div className="row">
                                    <div className="col-xl-5 col-lg-5 col-md-5 col-sm-4">
                                        <div className="text-center font-weight-bold  list-group">
                                            <div className="list list-group-item text-white" style={isLargeScreen ? label.large : label.normal}>RANK</div>
                                            <div className="list list-group-item" style={isLargeScreen ? score_style.large : score_style.normal} >{props.rank}</div>

                                        </div>
                                    </div>
                               
                                    <div className="col-xl-7 col-lg-7 col-md-7 col-sm-4">
                                      <div className="text-center font-weight-bold text-white list-group">
                                            <div className="list list-group-item" style={isLargeScreen ? label.large : label.normal}>SCORE</div>
                                          <div className=" list list-group-item" style={isLargeScreen ? score_style.large : score_style.normal}>{props.score}</div>

                                      </div>
                                           
                                    </div>
          
                                  </div>
                               
                                </div>
    
                          <div>
                            
                             <div className="d-flex justify-content-center m-2 p-2">
                                <img alt="graph" src={icon} style={{width: '25%', position: 'relative', opacity: '0.5'}} />
                             </div>
                          </div>    
                </Card>
                
            
       
    )
}
    



export default UserPanel