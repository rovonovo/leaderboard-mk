import React from 'react'
import dp from "assets/profile.svg"


function MobwinnerPanel(props) {
    return (
       <div>
        <div className="media p-1 m-4 media-shaper">
             
                  <div className="mobile-score">
                      
                           <div className="mob-inner-circle text-white">
                               <img src={props.cup} style={{width : '3.7rem'}}/>
                         
                      </div>
                  </div>    
             
     
         <div className="media-body mt-1 ">
            <div className="row ml-2" >
            
                <div className="col-sm-12 col-md-12 col-lg-12 text-white" style={{fontSize:'1rem',color: '#fcfbdb'}}> {props.name.toUpperCase()}   
                    <div style={{borderBottom:'1px solid #14c048', opacity:'0.2', marginTop:'4px'}}></div>

               </div>
                
               
                <div className="col-sm-12 col-md-12 col-lg-12">
                    <div  className="offset-lg-4 offset-md-3">
                        <div className="row font-weight-bold">
                         <div className="col-12" style={{fontSize:'1.4rem',color: '#fcfbdb'}}>
                            {props.score}

                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
         </div>
         <p className="text-white p-2 pl-1" style={{fontSize:'1.2rem'}}>{props.rank}
               
        </p>
       </div>
       </div> 
    )
}

export default MobwinnerPanel
