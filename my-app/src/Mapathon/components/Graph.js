import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine,
} from 'recharts';


export default class Example extends PureComponent {
    
    
  render(props) {
  
    return (
     <div style={{backgroundImage: 'linear-gradient(to bottom, #071929, #0b3326)' ,borderRadius: '10px', boxShadow:'1px 2px 4px 1px grey'}}>
      <LineChart width={this.props.width} height={260} data={this.props.data} style={{color: '#2eb8b8'}}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="display_name" padding={{ left: 30, right: 30 }}/>
        <YAxis />
        <Tooltip/>
        <Legend/>
        <Line type="monotone" dataKey="score" stroke="#00cc88" activeDot={{ r: 10 }} strokeWidth= {2}/>
        <Line legendType={this.props.legend} type="monotone" dataKey={this.props.userline} stroke="#ff5050" activeDot={{ r: 10 }} strokeWidth= {2}/>
      </LineChart>
      </div>   
    );
  }
}
