import React from 'react'

function Entity(props) {
    return (
        <div className="d-block text-center m-2 p-2" style={props.style}>
           <h5 className="text-dark pt-1">{props.title}</h5>
            <h2 className="text-success font-weight-bold m-1 p-2">{props.content}</h2>
        </div>
    )
}

export default Entity
