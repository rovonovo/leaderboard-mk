import React from 'react'
import "./leaderboard.css"
import {useMediaQuery} from "react-responsive"

function WinnersPanel(props) {

  const isLargeScreen = useMediaQuery({
    query: '(min-width: 1900px)'
  })

  let player_style=  {
    normal: {letterSpacing:'1px', fontSize:'1rem', fontFamily: '"Trebuchet MS", Helvetica, sans-serif', color:'#fcfbdb'},
    large :  {letterSpacing:'2px', fontSize:'1.6rem', fontFamily: '"Trebuchet MS", Helvetica, sans-serif',color:'#fcfbdb'}
  }
  let score_style=  {
    normal: {fontSize:'1.8rem', color: '#fcfbdb'},
    large :  {fontSize:'2.5rem', color: '#fcfbdb'}
  }
  let cup_style = {
    normal:  {width : '5rem'} ,
    large :   {width: '7.5rem'}
  }
  

    return (
        <div className="container-fluid justify-content-around" style={isLargeScreen ? {margin : '6px', padding: '20px'} : {margin: '2px' ,padding: '6px'}}>
        <div className="media p-1 m-4 " style={props.media}>
       
         <div className="media-body mt-1">
            <div className="row ml-2" >
            
               <div className="col-md-1 col-lg-1 " >
               <p className="text-white" style={isLargeScreen ? {fontSize:'2.6rem'} : {fontSize:'2rem'}}>{props.rank}
                <sup  style={isLargeScreen ? {opacity: '0.6', fontSize:'1.3rem'} : {opacity: '0.6', fontSize:'1rem'}}>{props.super}</sup></p>
                 
               </div>
                <div className="col-sm-2 col-md-3 col-lg-3">
                  
                            <div className="circle-score">
                                
                                     <div className="inner-circle text-white">
                                         <img src={props.cup} style={isLargeScreen ? cup_style.large : cup_style.normal }
                                             alt="Trophy"
                                         />
                                   
                                </div>
                            </div>    
                       
                </div>
                <div className="col-sm-12 col-md-4 col-lg-7  ml-2 pl-4"> <span style={isLargeScreen ? player_style.large : player_style.normal}>{props.name.toUpperCase()}</span>
                        
                            <div style={{borderBottom:'1px solid rgb(3, 5, 5)', opacity:'0.3', marginTop: '2px'}}></div>
                            <div className="row pl-3 text-center">
                                <div style={isLargeScreen ? score_style.large : score_style.normal}>{props.score}
                                                                
                                </div>
                         </div>
                </div>
            </div>
         </div>
       </div>
       </div>
    )
}

export default WinnersPanel

/*<img src={dp} alt="profile" className="p-2 rounded" style={isLargeScreen ? {width:'3rem'} : {width:'2.5rem'}} />*/